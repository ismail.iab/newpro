<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iCreation Project</title>
    <link rel="stylesheet" href="dist/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="dist/lib/font-awesome-4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="dist/css/custom.css">
    <link rel="stylesheet" href="dist/css/style.css">
    </head>
<body>
    
<div class="container">
<div class="row">
<div class="col-md-6"><br><br>

<form action="sort.php" method="POST">

  <div class="form-group">
  <label for="exampleInputN1">Number 1</label>
    <input type="number" name="num1" class="form-control" placeholder="Number 1" required>
  </div>
  
  <div class="form-group">
  <label for="exampleInputN2">Number 2</label>
    <input type="number" name="num2" class="form-control" placeholder="Number 2" required>
  </div>

  <div class="form-group">
  <label for="exampleInputN3">Number 3</label>
    <input type="number" name="num3" class="form-control" placeholder="Number 3" required>
  </div>

  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
</form>
    <script src="dist/lib/jquery/jquery.js"></script>
    <script src="dist/lib/bootstrap/js/bootstrap.js"></script>
</body>
</html>