<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iCreation Project</title>
    <link rel="stylesheet" href="dist/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="dist/lib/font-awesome-4.7.0/css/font-awesome.css">
    <link rel="stylesheet" href="dist/css/custom.css">
    <link rel="stylesheet" href="dist/css/style.css">
    </head>
<body>
    
<div class="container">
<div class="row">
<div class="col-md-6"><br><br>
<?php
if(isset($_POST['submit'])){

$name = $_POST['name'];
$roll = $_POST['roll'];

echo "Registration Successfully!<br>Name: $name <br> Roll: $roll";
}
?>

<form action="" method="POST">

  <div class="form-group">
  <label for="exampleInputName">Name</label>
    <input type="text" name="name" class="form-control" placeholder="Name" required>
  </div>
  
  <div class="form-group">
  <label for="exampleInputEmail">Email</label>
    <input type="email" name="email" class="form-control" placeholder="Email" required>
  </div>

  <div class="form-group">
  <label for="exampleInputRoll">Roll</label>
    <input type="number" id="limit" maxlength="6" name="roll" class="form-control" placeholder="Roll" required>
  </div>
  <div class="form-group">
  <label for="exampleInputMobile">Mobile</label>
    <input type="number" name="mobile" class="form-control" placeholder="Mobile" required>
  </div>
  <div class="form-group">
  <label for="exampleInputN3">Address</label>
  <textarea class="form-control" name="address" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>

  <button type="submit" name="submit" class="btn btn-primary">Registration</button>


</form>
    <script src="dist/lib/jquery/jquery.js"></script>
    <script src="dist/lib/bootstrap/js/bootstrap.js"></script>
</body>
</html>